# About Kali Linux Custom DWM Openbox i3wm edition by Jacek 


|Feature           |Description                                                                         |
|------------------|------------------------------------------------------------------------------------|
|GUI               |DWM (stock or custom DWM) or Openbox or i3wm                                        |
|Login mode        |lightdm                                                                             |
|DWM stock ModKey  |Alt                                                                                 |
|DWM custom ModKey |Win                                                                                 |
|i3wm ModKey       |Win                                                                                 |
|Openbox ModKey    |Win                                                                                 |
|DWM terminal      |ModKey + Shift + Enter (Win + Shift + Enter)                                        |
|DWM Dmenu         |ModKey + p (Win + p)                                                                |
|Network Manager   |nmtui, nm-applet                                                                    |
|Volume Control    |pavucontrol                                                                         |
|Power Manager     |mate-power-manager, upower -i /org/freedesktop/UPower/devices/battery_BAT0          |
|Openbox terminal  |Win + Enter                                                                         |
|Openbox Dmenu     |Alt + F3                                                                            |
|i3wm terminal     |Win + Enter                                                                         |
|i3wm Dmenu        |Win + d                                                                             |
|WWW browser       |Firefox Developer Edition, run Dmenu and type firefox.sh                            |
|tox client        |toxic                                                                               |

* Main gui applications and packages: 

```
#text editors
pluma
geany

#office apps
gpicview
evince
galculator
xarchiver

#terminals
xterm 
mate-terminal
terminator

#web apps
chromium
falkon
youtube-dl

#development
meld
git

#file managers
ranger
caja

#system tools 
lxrandr
lxinput
pulseaudio
udiskie
network-manager
network-manager-gnome
clipit
mate-power-manager
xscreensaver
mate-system-monitor

#audio video
mpv
smplayer
audacious

#other
plank


```

* Full list of packages for variant: [dwm_openbox_i3](./variant-dwm_openbox_i3/package-lists/kali.list.chroot)









